FROM resin/beaglebone-green-wifi-node:6

# Defines our working directory in container
WORKDIR /usr/src/app

# Copies the package.json first for better cache on later pushes
COPY package.json package.json
COPY . ./
# This install npm dependencies on the resin.io build server,
# making sure to clean up the artifacts it creates in order to reduce the image size.
RUN JOBS=MAX npm install \
              && npm prune --production \
              && npm cache clean \
              && rm -rf /tmp/*

CMD ["node", "index.js"]
