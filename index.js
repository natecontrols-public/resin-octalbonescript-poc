const Bonescript = require('octalbonescript')

// 1. Enable UART4.
Bonescript.serial.enable('/dev/ttyO4', (err) => {
  if (err) {
    console.error('Error opening serial port', err)
  } else {
    console.log('Serial port opened.')
  }
})

// 2. Set GPIO input pin.
Bonescript.pinMode('P9_23', 'in', (err, pin) => {
  if (err) {
    console.error('Error initializing GPIO in', err)
  } else {
    console.log('GPIO in opened.')
  }
})
